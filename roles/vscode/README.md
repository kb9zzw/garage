# role vscode

Installs Microsoft Visual Studio Code.

### Ubuntu / Debian

* vscode_repo: DEB repo for vscode
* vscode_repo_key: DEB repo key for vscode

### CentOS / RedHat / Amazon

* vscode_repo_baseurl: YUM repo for vscode
* vscode_repo_gpgkey: YUM repo key for vscode

### MacOSX

No config.  Installs with Homebrew Cask.
