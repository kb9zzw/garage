# role r

Installs R and R-related development tools

### RedHat / CentOS / Amazon Linux

* rdev_packages: The list of OS packages to install.
* rdev_rstudio_enabled: Install RStudio?  Default true.
* rdev_rstudio_version: The version of RStudio to install.
* rdev_rstudio_package: The RStudio package URL

### Debian / Ubuntu

* rdev_packages: The list of packages to install.
* rdev_rstudio_enabled: Install RStudio?  Default true.
* rdev_rstudio_version: The version of RStudio to install.
* rdev_rstudio_package: The RStudio package URL

### MacOSX

* rdev_packages: The list of homebrew packages to install.
* rdev_rstudio_enabled: Install RStudio? Default true.
